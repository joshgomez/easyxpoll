<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?=((!empty($question)) ? sanitizeSpChars($question) .' - ' : ''). DOCUMENT_TITLE?></title>
		<link rel="stylesheet" href="<?=$g_hostURL?>/styles/poll-main.min.css" />
		<link rel="stylesheet" href="<?=$g_hostURL?>/styles/poll-theme-<?=rawurlencode($theme['css'])?>.min.css" />
		<link rel="stylesheet" href="<?=$g_hostURL?>/styles/ionicons.min.css" />
		<link rel='icon' type='image/vnd.microsoft.icon' href='<?=$g_hostURL?>/favicon.ico' />
	</head>
	<body>
		<?php if($id): ?> 
		<?php require __DIR__ . '/poll.template.php';?>
		<?php else: ?>
		<p>
			<?=_translate('TXT_POLL_EXISTS_NONE')?>
			<br />
			<a href="<?=$g_hostURL?>" target='_blank'><?=_translate('LINK_CREATE')?></a>
		</p>
		<?php endif; ?>
	</body>
</html>