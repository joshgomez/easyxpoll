<?php 

	$g_htmlDescription 	.= ' '. _translate('HTML_DESCRIPTION_POLL_EDIT');

	$haveAccess = false;
	$id = ifSetOr($_REQUEST['id']);
	
	if(!$authorized)
		$poll->getById($id);
		
	if($poll->id() && $poll->hidden() === 0)
	{
		$cipher = new Cipher(ENCRYPTION_KEY);
		$accesskey = $cipher->decrypt($poll->accesskey());
	
		if(!$authorized)
		{
			$key = ifSetOr($_REQUEST['key']);
			if($poll->ip() === inet_pton(getIP()) || $accesskey  === $key)
			{
				$session->setPoll($poll->id());
				headerRedirect($g_hostURL.'/page/poll');
			}
		}
		else
		{
			$haveAccess = true;
			$g_title = $poll->question();
			
			$iframeHTML = 	'';
			$iframeHTML .= 	'<iframe frameborder="0" allowtransparency="true" ';
			$iframeHTML .= 	'style="width:100%" ';
			$iframeHTML .= 	'src="'.$g_hostURL.'/id/'.$poll->id().'" seamless>'; 
			$iframeHTML .= 	'</iframe>';
			
			$edit = ifSetOr($_POST['edit']);
			$delete = ifSetOr($_POST['delete']);
			if($edit)
			{
				$theme = ifSetOr($_POST['theme']);
				$close = ifSetOr($_POST['close']);
				$poll->edit($theme,$close);
			}
			else if($delete)
			{
				$poll->delete();
				$session->destroy();
				headerRedirect($g_hostURL);
			}
			
			$closeInputs = [1 => _translate('YES'), 0 => _translate('NO')];
		}
	}