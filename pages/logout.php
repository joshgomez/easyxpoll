<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}

	$g_title = _translate('LOGOUT');
	$session->destroy();
	headerRedirect($g_hostURL);
	
	