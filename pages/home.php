<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}

	$g_title = '';
	$g_htmlDescription 	.= ' '. _translate('HTML_DESCRIPTION_HOME');
	
	$question = '';
	$answers = [];
	
	$descQuestion 		= '';
	$descAnswer 		= '';
	$descCaptcha 		= '';
	
	$classQuestion		= '';
	$classAnswer		= '';
	$classCaptcha		= '';
	
	$selectedAnswers = (int)ifSetOr($_POST['answers']);
	$selectedAnswers = min(COUNT_MAX_OPTIONS,$selectedAnswers);
	$selectedAnswers = max(2,$selectedAnswers);
	
	$selectedTheme = (int) ifSetOr($_POST['theme']);
	
	$submitPoll = ifsetor($_POST['create']);
	if($submitPoll){
	
		$question = ifsetor($_POST['question']);
		$answers = ifsetor($_POST['answer']);
		$captcha = ifsetor($_POST['captcha']);
	
		$created = $poll->create($question,$selectedAnswers,$answers,$captcha,$selectedTheme);
		if($created)
		{
			headerRedirect($g_hostURL.'/page/poll/'.$created);
		}
		
		$error = $poll->getFirstError('question');
		if($error) $classQuestion = ' failure';
		$descQuestion = ($error) ? $error : $descQuestion;
		
		$error = $poll->getFirstError('answer');
		if($error){
		
			$classAnswer = ' failure';
			$fAnswer = $poll->getErrorMessages('answer')['answer_field'];
			$descAnswer =  $error.' ('._translate('ANSWER').' '.$fAnswer.')';
		}
		
		$error = $poll->getFirstError('captcha');
		if($error) $classCaptcha = ' failure';
		$descCaptcha = ($error) ? $error : $descCaptcha;
		
		$question = sanitizeSpChars($question);
	}
	
	for($i = 0;$i < COUNT_MAX_OPTIONS;$i++)
	{
		$answers[$i] = ifSetOr($answers[$i],'');
		$answers[$i] = sanitizeSpChars($answers[$i]);
	}

	