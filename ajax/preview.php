<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	header('Content-Type: text/html; charset=utf-8');
	
	$question = ifSetOr($_POST['question']);
	$answers = ifSetOr($_POST['answer'],[]);
	
	$question = ($question === '') ? _translate('UNDEFINED').'?' : $question;
	
	$id = 0;
	$votes = [];
	$totalVotes = 0;
	
	$hasVoted = mt_rand(0,1);
	$closed = mt_rand(0,1);
	$disabled = true;
	
	foreach($answers as $i => $answer)
	{
		$answers[$i] = ifSetOr($answer);
		$answers[$i] = ($answers[$i] === '') ? _translate('UNDEFINED') : $answers[$i];
		
		$votes[$i] = mt_rand(0,1000);
	}
	
	$totalVotes = array_sum($votes);
	
	require __DIR__ .'/../poll.template.php';

?>

