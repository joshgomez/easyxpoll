<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	class PollStatistics extends Main
	{
		private $polls_created 	= 0;
		private $last_poll 		= 0;
		
		function __construct($id = null)
		{
			parent::__construct();
			
			$statistics = $this->dbHelper->findAllStatistics();
			if($statistics){
			
				foreach($statistics as $stats)
				{
					if(property_exists($this,$stats['name']))
						$this->$stats['name'] = $stats['value'];
				}
			}
		}
		
		public function polls_created(){return $this->polls_created;}
		public function last_poll(){return $this->last_poll;}
	}
	
	class Poll extends Main
	{
		private $_id = '';
		
		private $ip = '';
		private $accesskey = '';
		
		private $question = '';
		private $theme = 0;
		private $answers = [];
		private $votes = [];
		
		private $closed = 0;
		private $hidden = 0;
		
		private $updated = 0;
		private $created = 0;
		
		private $hasVoted = false;
		private $stats = null;
		
		private function ifValueSetProperty(array $values)
		{
			foreach($values as $property => $value)
			{
				if(property_exists($this,$property))
					$this->$property = $value;
			}
		}
		
		private function checkVoted()
		{
			$voted = $this->dbHelper->findVoteByIp($this->_id);
			if($voted)
				return true;
			
			return false;
		}
		
		function __construct($id = null,$stats = null)
		{
			parent::__construct();
			$this->getById($id);
			
			if($stats instanceof PollStatistics)
				$this->stats = $stats;
		}
		
		public function id(){return $this->_id;}
		public function ip(){return $this->ip;}
		public function accesskey(){return $this->accesskey;}
		public function question(){return $this->question;}
		public function theme(){return $this->theme;}
		public function answers(){return $this->answers;}
		public function votes(){return $this->votes;}
		public function closed(){return $this->closed;}
		public function hidden(){return $this->hidden;}
		public function updated(){return $this->updated;}
		public function created(){return $this->created;}
		
		public function hasVoted(){return $this->hasVoted;}
		
		public function getById($id)
		{
			$poll = $this->dbHelper->findPollById($id);
			if($poll){
			
				$this->ifValueSetProperty($poll);
				$this->hasVoted = $this->checkVoted();
				return $poll;
			}
			
			return false;
		}
		
		public function create($question,$total,array $answers,$secureimage,$theme = 0)
		{
			$created = false;
		
			$errors_question = getLengthError($question,'question');
			if(!empty($errors_question)){
			
				$this->setErrorMessages($errors_question,'question');
				return false;
			}
			
			$answers = array_slice($answers,0,$total);
			foreach($answers as $i => $answer)
			{
				$errors_answer = getLengthError($answer,'answer');
				if(!empty($errors_answer)){
				
					$errors_answer['answer_field'] = ($i + 1);
					$this->setErrorMessages($errors_answer,'answer');
					return false;
				}
			}
			
			require_once __DIR__ . '/../securimage/securimage.php';
			$securimage = new Securimage();
			$securimageValid = $securimage->check($secureimage);
			if(!$securimageValid)
			{
				$this->setErrorMessages(_translate('TXT_INVALID_CAPTCHA'),'captcha');
				return false;
			}
			
			if(!($this->stats instanceof PollStatistics))
				$this->stats = new PollStatistics();
			
			$polls = $this->stats->polls_created() + 1;
			if(!$this->dbHelper->updatePollStatsOnCreate($polls))
				return false;
				
			if(!array_key_exists($theme,Configs::pollColorThemes()))
				$theme = 0;
				
			$question = $this->dbHelper->insertPoll($question,$answers,$theme);
			if($question)
				return $question;
	
			return false;
		}
		
		public function vote($answer)
		{
			if($this->hasVoted)
				return false;
		
			try
			{
				$this->dbHelper->insertUserVote($this->_id,$answer);
				$this->dbHelper->updatePollVotes($this->_id,$answer,$this->votes);
			}
			catch(Exception $e){return false;}
			
			return true;
		}
		
		public function delete()
		{
			try
			{
				$this->dbHelper->removePoll($this->_id);
			}
			catch(Exception $e){return false;}
			
			return true;
		}
		
		public function edit($theme, $closed)
		{
			$theme = $theme;
			if(!array_key_exists($theme,Configs::pollColorThemes()))
				$theme = 0;
		
			switch($closed)
			{
				case 1 	: $closed = 1; break;
				default : $closed = 0;
			}
		
			if(!$this->dbHelper->updatePoll($this->_id,$theme,$closed))
				return false;
				
			$this->theme 	= $theme;
			$this->closed 	= $closed;
			return true;
		}
	}

