<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	function vTextContent($str,$pattern){
	
		$pattern = ifsetor($pattern,'');
		switch($pattern)
		{
			case "numbers"		: 	$pattern = "/\p{N}/u";
				break;
			case "letters"		: 	$pattern = "/\p{L}/iu";
				break;
			case "uppercase"	: 	$pattern = "/\p{Lu}/u";
				break;
			case "lowercase"	: 	$pattern = "/\p{Ll}/u";
				break;
			case "symbols"		: 	$pattern = "/\p{S}/u";
				break;
		}
		
		if(preg_match($pattern,$str))
			return true;
		
		return false;
	}

	function vTextLength($str,$maxLength,$minLength = 1){
	
		$str = (string) $str;
		$length = mb_strlen($str);
		
		if($length < $minLength)
			return 1;
		else if($length > $maxLength)
			return 2;

		return 0;
	}
	
	function getLengthError($str,$input)
	{
		$errors = [];
		$input = ifsetor($input);
		
		switch($input)
		{
			case 'question':
				$length_max = LENGTH_MAX_QUESTION;
				$length_min = LENGTH_MIN_QUESTION;
				$inputname	= 'QUESTION';
				break;
			case 'answer':
				$length_max = LENGTH_MAX_ANSWER;
				$length_min = LENGTH_MIN_ANSWER;
				$inputname	= 'ANSWER';
				break;
			default:
				$input = '';
		}
		
		if($input){
		
			$errors_length = vTextLength($str,$length_max,$length_min);
			if($errors_length == 1) $errors[] = sprintf(_translate('TXT_INPUT_TOO_SHORT'),_translate($inputname,'LC'),$length_min);
			else if($errors_length == 2) $errors[] = sprintf(_translate('TXT_INPUT_TOO_LONG'),_translate($inputname,'LC'),$length_max);
			
			return $errors;
		}
		
		$errors[] = sprintf(_translate('TXT_CODING_ERROR'),'getLengthError()');
		return $errors;
	}
	
	