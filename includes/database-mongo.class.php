<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	class DatabaseHandlerMongo extends DatabaseHelper
	{
		private $mongo = null;
	
		function __destruct()
		{
			$connections = $this->mongo->getConnections();

			foreach($connections as $link)
			{
				if($link['connection']['connection_type_desc'] == "SECONDARY")
				{
					$closed = $this->mongo->close($link['hash']);
				}
			}
		}
		
		protected function install()
		{
			$collectionNames = $this->handler->getCollectionNames();
			
			$collections = [
				[
					'name' 		=> DB_COLLECTION_POLLS,
					'options'	=>
					[
						'capped' 		=> true,
						'size' 			=> 1000*1024,
						'max' 			=> 1000,
						'autoIndexId' 	=> true
					],
					
					'index'		=>
					[
						"question" => 1
					]
				],
				[
					'name' 		=> DB_COLLECTION_POLL_VOTES,
					'options'	=>
					[
						'capped' 		=> false,
						'autoIndexId' 	=> true
					],
					
					'index'		=>
					[
						"id_poll" => 1,
						"ip" => 1
					],
					
					'index_options'		=>
					[
						"unique" => true
					]
				],
				[
					'name' 		=> DB_COLLECTION_POLL_STATISTICS,
					'options'	=>
					[
						'capped' 		=> false,
						'autoIndexId' 	=> true
					],
					
					'index'		=>
					[
						"name" => 1
					],
					
					'index_options'		=>
					[
						"unique" => true
					]
				]
			];
			
			foreach($collections as $collection)
			{
				if(in_array($collection['name'],$collectionNames)) continue;
				
				$install = $this->handler->createCollection($collection['name'],$collection['options']);
				if($install){
					
					if(isset($collection['index']))
					{
						$indexOptions = [];
						if(isset($collection['index_options']))
							$indexOptions = $collection['index_options'];
						
						$install->ensureIndex($collection['index'],$indexOptions);
					}
					
					if($collection['name'] == DB_COLLECTION_POLL_STATISTICS)
					{
						$count 	= 0;
						$time 	= 0;
						
						$this->bindType($count,'i');
						$this->bindType($time,'t');
						
						$data = ['name' => 'polls_created', 'value' => $count];
						$this->insert($collection['name'],$data);
						
						$data = ['name' => 'last_poll', 'value' => $time];
						$this->insert($collection['name'],$data);
					}
				}
			}
			
			$collectionNames = $this->handler->getCollectionNames();
			if(count($collectionNames) === count($collections))
			{
				$text = "Do only remove this file when you must reinstall the database.";
				if(file_put_contents(__DIR__ .'/../installed/mongodb.txt',$text))
					return true;
			}

			return false;
		}
		
		public function connect()
		{
			$login = '';
			if(parent::user())
				$login = sprintf('%s:%s@',parent::user(),parent::password());
				
			if(parent::port())
				$port = ':'.parent::port();
		
			$mongo_connection = sprintf('%s://%s%s%s',parent::type(),$login,parent::host(),$port);
			
			$this->mongo = new MongoClient($mongo_connection);
			if($this->mongo)
			{
				$this->handler = $this->mongo->selectDB(parent::database());
				$this->connected = true;
				
				if(!file_exists(__DIR__ . '/../installed/mongodb.txt'))
				{
					$this->install();
				}
				
				return true;
			}
			
			$this->setLastError();
			return false;
		}
		
		public function getKey($name)
		{
			if(($name instanceof MongoCollection))
				return $name;

			$collection = $this->handler->selectCollection($name);
			if($collection)
			{
				return $collection;
			}
			
			return false;
		}
		
		public function get($collection,array $query, $fields = null,$all = false)
		{
			$collection = $this->getKey($collection);
			if($collection){
			
				if(!is_array($fields)) $fields = [];
			
				$result = ($all) ? $collection->find($query,$fields) : $collection->findOne($query,$fields);
				if($result)
					return $result;
			}
			
			$this->setLastError();
			return false;
		}
		
		public function set($collection,array $query, $options = null)
		{
			$collection = $this->getKey($collection);
			if($collection){
			
				if(!is_array($options)) $options = [];
			
				$set = $collection->save($query,$options);
				if($set)
					return $set;
			}
			
			$this->setLastError();
			return false;
		}
		
		public function insert($collection,array $query, $options = null)
		{
			$collection = $this->getKey($collection);
			if($collection){
			
				if(!is_array($options)) $options = [];
			
				$query['_id'] = new MongoId();
				$insert = $collection->insert($query,$options);
				if($insert)
					return $query['_id'];
			}
			
			$this->setLastError();
			return false;
		}
		
		public function remove($collection,array $query, $options = null)
		{
			$collection = $this->getKey($collection);
			if($collection){
			
				if(!is_array($options)) $options = [];
			
				if($collection->remove($query,$options))
					return true;
			}
			
			$this->setLastError();
			return false;
		}
		
		public function update($collection,array $query, array $data, $options = null)
		{
			$collection = $this->getKey($collection);
			if($collection){
			
				if(!is_array($options)) $options = [];
			
				$data = ['$set' => $data];
				if($collection->update($query,$data,$options))
					return true;
			}
			
			$this->setLastError();
			return false;
		}
		
		public function getLength($collection,array $query, $limit = 0, $skip = 0)
		{
			$collection = $this->getKey($collection);
			if($collection)
			{
				$count = $collection->count($query,$limit,$skip);
				if($count !== false)
					return $count;
			}
			
			$this->setLastError();
			return false;
		}
		
		public function bindType(&$value,$type)
		{
			switch($type)
			{
				case 'a':
					$value = (array)$value;
					break;
				case 'b':
					$value = (bool)$value;
					break;
				case 'i':
					$value = (int)$value;
					break;
				case 'I':
					$value = (string)$value;
					try {$value = new MongoId($value);}
					catch(MongoException $ex){$value = false;}
					break;
				case 'l':
					try {$value = new MongoBinData($value,0);}
					catch(MongoException $ex){$value = false;}
					break;
				case 's':
					$value = (string)$value;
					break;
				case 't':
					$value = (int)$value;
					try {$value = new MongoDate($value);}
					catch(MongoException $ex){$value = false;}
					break;
				default:
					$value = false;
			}
			
			if($value !== false || $type === 'b')
			{
				return $value;
			}
			
			return false;
		}
		
		public function getArray(array $data)
		{
			$callback = function($value){
			
				if($value instanceof MongoId)
					return $value->{'$id'};
					
				if($value instanceof MongoBinData)
					return $value->{'bin'};
					
				if($value instanceof MongoDate)
					return $value->{'sec'};
					
				return $value;
			};
			
			return array_map($callback,$data);
		}
		
		public function setLastError()
		{
			if(!$this->connected) return false;
			
			$errorInfo = $this->handler->lastError();
			if($errorInfo)
			{
				$this->errno = $errorInfo['err'];
				$this->error = $errorInfo['n'];
			}
			
			return true;
		}
	}
	
	