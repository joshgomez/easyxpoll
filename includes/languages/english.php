<?php

	if(!IN_SITE)
	{
		exit;
	}
	
	$lang['HTML_KEYWORDS'] = "EasyXPoll,Poll,Polls,Internet,XzaR,Josh,Xuniver,Question,Answer,Easy,Simple,Free";
	$lang['HTML_DESCRIPTION'] = "A simple poll creation service where you can embed polls on your website.";
	$lang['TXT_SLOGAN'] = "a simple poll creation service.";
	
	$lang['HTML_DESCRIPTION_HOME']			= "This is the front page of EasyXPoll. Create your poll here.";
	$lang['HTML_DESCRIPTION_POLL_EDIT'] 	= "This is the page where you can edit and view your poll.";
	$lang['HTML_DESCRIPTION_POLL']			= "This is a poll created with EasyXPoll.";
	
	$lang['QUESTION'] = "Question";
	$lang['ANSWER'] = "Answer";
	$lang['THEME'] = "Theme";
	$lang['ANSWERS'] = "Answers";
	$lang['DELETE'] = "Delete";
	$lang['CREATE'] = "Create";
	$lang['EDIT'] = "Edit";
	$lang['VOTE'] = "Vote";
	$lang['SELECT'] = "Select";
	$lang['LOADING'] = "Loading";
	$lang['PREVIEW'] = "Preview";
	$lang['SETTINGS'] = "Settings";
	$lang['CLOSE'] = "Close";
	$lang['YES'] = "Yes";
	$lang['NO'] = "No";
	
	$lang['LOGOUT'] = "Logout";
	
	$lang['YOUR_POLL'] = "Your Poll";
	$lang['YOUR_CODE'] = "Your Code";


	$lang['CAPTCHA'] = "Captcha";
	$lang['PASSWORD'] = "Password";
	$lang['ENTER_CODE'] = "Enter Code";
	$lang['OPTIONAL'] = "Optional";
	$lang['STATISTICS'] = "Statistics";
	
	$lang['POLLS_CREATED'] = "Polls Created";
	$lang['LAST_POLL'] = "Last Poll";
	
	$lang['LAST_VOTED'] = "Last Voted";
	$lang['DATE_CREATED'] = "Date Created";
	
	$lang['NONE'] = "None";
	
	$lang['VOTES'] = "Votes";
	
	$lang['ID'] = "Id";
	$lang['KEY'] = "Key";
	
	$lang['TXT_SPAM'] = "Please don't abuse the refresh command or double click on links many time in row. <a href=''>Go Back</a><br />Spammed: %d Total: %d";
	
	$lang['TXT_SAME_AS'] = "Same as %s.";
	
	$lang['TXT_DB_ERROR'] = "DB->Error: (#%d) %s";
	$lang['TXT_DB_PUBLIC_ERROR'] = "DB->Error: Something went wrong!";
	
	$lang['TXT_INVALID_CAPTCHA'] = "Invalid captcha code entered.";
	$lang['TXT_CAPTCHA_PLACEHOLDER'] = "Are you human?";
	
	$lang['TXT_PASSWORD_WRONG'] = "Your password is wrong.";

	$lang['TXT_VALID_USERNAME'] = "Letters and numbers allowed followed by one underscore or hyphen.";

	
	$lang['TXT_MIN_LENGTH'] = "Minimum length %d characters.";
	$lang['TXT_MAX_LENGTH'] = "Maximum length %d characters.";
	
	$lang['TXT_INPUT_TOO_SHORT'] = "The %s you have entered is too short. Minimum length %d characters.";
	$lang['TXT_INPUT_TOO_LONG'] = "The %s you have entered is too long. Maximum length %d characters.";
	
	$lang['TXT_CODING_ERROR'] = "Something went wrong! Function: %s.";
	$lang['TXT_POLL_EXISTS_NONE'] = "The poll does not exists.";
	
	$lang['LINK_CREATE'] = "Create one, free and easy!";
	$lang['TXT_POLL_KEY'] = "Your key is <strong>%s</strong>.";
	$lang['TXT_VOTED'] = "Thank you for your vote!";
	$lang['TXT_CLOSED'] = "The poll has been closed.";
	
	$lang['TXT_POLL_NONE'] = "The poll could not be found.";
	$lang['UNDEFINED'] = "Undefined";
	
	
