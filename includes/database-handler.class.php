<?php
	
	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	require_once __DIR__ . '/database-helper.class.php';
	require_once __DIR__ . '/database-mongo.class.php';
	
    class DatabaseFactory
    {
		static private $instances = [];
		static private $index = '';

		private function __construct(){}
		private function __clone(){}

		static public function create($type,$index = 'master',$host = '',$port = '',$password = '',$user = '',$database = '',$charset = '',$databaseType = '')
		{
			if(empty(static::$instances[$index]))
			{
	            switch($type)
	            {
	                case Configs::DBEXT_MONGO:
						static::$instances[$index] = new DatabaseHandlerMongo();
						break;
	            }
				
				end(static::$instances);
				static::$index = key(static::$instances);
				
				if(static::$instances[static::$index])
				{
					static::$instances[static::$index]->config($host,$port,$password,$user,$database,$charset,$databaseType);
					static::$instances[static::$index]->connect();
				}
			}
			else
			{
				static::$index = $index;
			}
		}

		static public function getIndex(){return static::$index;}
		
		static public function getIndexes(){return array_keys(static::$instances);}
		
		static public function getInstance($index = null)
		{
			if(!$index) $index = static::$index;
		
			if(array_key_exists($index,static::$instances))
				return static::$instances[$index];
				
			return false;
		}
		
		static public function closeInstance($index = null)
		{
			if(!$index) $index = $this->index;
		
			if(array_key_exists($index,static::$instances))
			{
				unset(static::$instances[$index]);
				return true;
			}
				
			return false;
		}
    }
	
	class DatabaseHandler
	{
		protected $handler = null;
		protected $connected = false;
		
		protected $errno = 0;
		protected $error = '';
	
		private $type 		= '';
		private $host 		= '';
		private $port 		= '';
		private $database	= '';
		private $charset	= '';
		private $user		= '';
		private $password 	= '';
		
		public function config($host,$port,$password,$user,$database,$charset,$type)
		{
			$this->type 		= ($type 		!= '')		? $type 	: DB_TYPE;
			$this->host  		= ($host 		!= '') 		? $host 	: DB_HOST;
			$this->port  		= ($port 		!= '') 		? $port 	: DB_PORT;
			$this->database		= ($database 	!= '') 		? $database : DB_DATABASE;
			$this->charset		= ($charset 	!= '') 		? $charset 	: DB_CHARSET;
			$this->user			= ($user 		!= '') 		? $user 	: DB_USER;
			$this->password		= ($password 	!= '') 		? $password	: DB_PASSWORD;
		}
		
		protected function type(){return $this->type;}
		protected function host(){return $this->host;}
		protected function port(){return $this->port;}
		protected function database(){return $this->database;}
		protected function charset(){return $this->charset;}
		protected function user(){return $this->user;}
		protected function password(){return $this->password;}
		
		public function outputError()
		{
			if($this->errno > 0)
			{
				if(DB_PRINT_ERROR)
				{
					$message = sprintf(_translate('TXT_DB_ERROR'),$this->errno,$this->error);
					return $message;
				}
				
				return _translate('TXT_DB_PUBLIC_ERROR');
			}
			
			return "";
		}
		
		public function resetError()
		{
			$this->errno = 0;
			$this->error = '';
			
			return true;
		}
		
		public function handler(){return $this->handler;}
		public function connected(){return $this->connected;}
	}

	abstract class DatabaseLayer extends DatabaseHandler
	{
		abstract public function connect();
		abstract public function getKey($name);
		abstract public function get($collection,array $query, $fields = null,$all = false);
		abstract public function set($collection,array $query, $options = null);
		abstract public function insert($collection,array $query, $options = null);
		abstract public function remove($collection,array $query, $options = null);
		abstract public function setLastError();
	}
	
