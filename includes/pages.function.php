<?php 

	if(!defined('IN_SITE'))
	{
		exit;
	}

	function page($name)
	{
		switch($name)
		{
			case 'logout':				$page = 'logout'; break;
			case 'poll':				$page = 'poll'; break;
			default:					$page = 'home';
		}
	
		return $page;
	}
	
	function pageAjax($name)
	{
		switch($name)
		{
			case 'preview': 				$page = 'preview'; break;
			default: 						$page = 'unauthorized';
		}
		
		return $page;
	}

