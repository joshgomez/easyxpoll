<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}

	function ifSetOr(&$var,$default = null){
	
		$temp = isset($var) ? $var : $default;
		return $temp;
	}
	
	function sanitizeSpChars($str){
	
		return filter_var($str,FILTER_SANITIZE_SPECIAL_CHARS);
	}
	
	function getHostURL(){
	
		$url = 'http';
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on")
			$url .= 's';
			
		$host = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';
		if($host === '')
			$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
			
		$url .= '://'.$host;
		$url = (BASE_URL != '/') ? $url.BASE_URL : $url;
		
		return $url;
	}
	
	function getCurrentURL()
	{
		$uri = $_SERVER['REQUEST_URI'];
		if(BASE_URL != '/')
			$uri = str_replace(BASE_URL,'',$_SERVER['REQUEST_URI']);
			
		return getHostURL().$uri;
	}
	
	function headerRedirect($url)
	{
		usleep(300000);
		header('Location: ' . $url);
		exit;
	}
	
	function _translate($index,$type = ''){
	
		require_once __DIR__ . '/main.php';
	
		$lang = [];
		$language = 'english';
		
		$page = ifSetOr($_GET['page'],'home');
		$page = page($page);
		
		require __DIR__ . '/languages/'. $language .'.php';
		$pageFile = __DIR__ . '/languages/'. $language . '/' . $page .'.php';
		if(file_exists($pageFile))
		{
			require $pageFile;
		}
		
		$str = isset($lang[$index]) ? $lang[$index] : 'undefined';
		switch($type)
		{
			case 'UC':
				$str = mb_strtoupper($str);
				break;
			case 'LC':
				$str = mb_strtolower($str);
				break;
		}
		
		return $str;
	}
		
	function getIP($secure = true){
	
		if($secure)
			return $_SERVER['REMOTE_ADDR'];
		
		$ip = '';
		if(isset($_SERVER['HTTP_CLIENT_IP']))
			$ip = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else
			$ip = $_SERVER['REMOTE_ADDR'];
			
		if(filter_var($ip,FILTER_VALIDATE_IP) !== false)
		{
			return $ip;
		}
	    
	    return false;
	}
	
	function setTimezone($ip = ''){
	
		if(!$ip) return false;
	
		$country =@ geoip_country_code_by_name($ip);
		if($country !== false){
		
			$timezone =@ geoip_time_zone_by_country_and_region($country);
			if($timezone !== false){
			
				$defaultTimezone = date_default_timezone_get();
				if(date_default_timezone_set($timezone) === false)
					date_default_timezone_set($defaultTimezone);
			}
		}
	}

	function is_spamming($page,$offset)
	{
		if(session_status() == PHP_SESSION_NONE){
			
			session_start();
		}
	
		$milliseconds = round(microtime(true) * 1000);
		if(empty($_SESSION[SESS_KEY_SPAM][$page])){
		
			$_SESSION[SESS_KEY_SPAM][$page] = array($milliseconds,1,0);
		}
		else
		{
			if(($milliseconds - $offset) < $_SESSION[SESS_KEY_SPAM][$page][0]){
			
				$_SESSION[SESS_KEY_SPAM][$page][1] += 1;
				$_SESSION[SESS_KEY_SPAM][$page][2] += 1;
				
				if($_SESSION[SESS_KEY_SPAM][$page][1] > 3)
					sleep(5);
				

				return[
				
					'spammed' 	=> $_SESSION[SESS_KEY_SPAM][$page][1], 
					'total'		=> $_SESSION[SESS_KEY_SPAM][$page][2]
				
				];
			}
			else
				$_SESSION[SESS_KEY_SPAM][$page][1] = 0;
			
			$_SESSION[SESS_KEY_SPAM][$page][0] = $milliseconds;
		}
		
		return false;
	}
	
	function setSpamFilter($page = 'main',$offset = 200)
	{
		$isSpamming = is_spamming($page,$offset);
		if($isSpamming){
		
			header('HTTP/1.1 400 Bad Request');
			die(sprintf(_translate('TXT_SPAM'),$isSpamming['spammed'],$isSpamming['total']));
		}
	}
	
	function randomPassword($length = 8)
	{
		$haystack = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$password = array();
		
		$haystackLength = mb_strlen($haystack) - 1;
		for($i = 0; $i < $length; $i++)
		{
			$randomIndex = mt_rand(0,$haystackLength);
			$password[] = $haystack[$randomIndex];
		}
		
		return implode($password);
	}
	
	function setCacheHeader($etag,$offset)
	{
		$time = time();
		
		header("Etag: ".$etag);
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s',$time) . ' GMT');
		header('Expires: ' . gmdate('D, d M Y H:i:s', $time + $offset) . ' GMT');
		header('Pragma: cache');
		header('Cache-Control: max-age=' . $offset . ', must-revalidate');
		header('Vary: Accept-Encoding');
	}
	
	function compressHTML($buffer){
	
		$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
		$buffer	= str_replace(': ', ':', $buffer);
		$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
		$buffer = ob_gzhandler($buffer, 9);
		
		return $buffer;
	}
	
	