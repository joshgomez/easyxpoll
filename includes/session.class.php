<?php

	if(!IN_SITE)
	{
		exit;
	}

	class Session extends Main
	{
		private function __clone(){}
		function __construct()
		{
			parent::__construct();
		}
		
		private function check()
		{
			$hashed_userAgent 	= hash('md5',$_SERVER['HTTP_USER_AGENT']);
			$hashed_remoteIP 	= hash('md5',getIP());
			
			if(empty($_SESSION[SESS_KEY_H_HTTPUSERAGENT]) || empty($_SESSION[SESS_KEY_H_REMOTEADDR])){
			
				$_SESSION[SESS_KEY_H_HTTPUSERAGENT] = $hashed_userAgent;
				$_SESSION[SESS_KEY_H_REMOTEADDR]	= $hashed_remoteIP;
			}
			
			if($_SESSION[SESS_KEY_H_HTTPUSERAGENT] !== $hashed_userAgent || $_SESSION[SESS_KEY_H_REMOTEADDR] !== $hashed_remoteIP){
			
				$this->destroy();
				return false;
			}
			
			return true;
		}
		
		public function setPoll($id)
		{
			if($id){
	
				session_regenerate_id(true);
				$_SESSION[SESS_KEY_POLLID] = $id;
				
				return true;
			}
			
			return false;
		}
		
		public function checkAuthorized()
		{
			$poll = ifSetOr($_SESSION[SESS_KEY_POLLID]);
			if($poll && $this->check())
				return $poll;
			
			return false;
		}
		
		public function destroy()
		{
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]);
			
			session_unset();
			session_destroy();
			
			return true;
		}
	}
	
	