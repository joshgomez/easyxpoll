<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}

	mb_internal_encoding('UTF-8');
	date_default_timezone_set('Europe/Stockholm');
	
	define('GOOGLE_ANALYTICS_ID', '');
	
	if(empty($_COOKIE["PHPSESSID"]))
		setcookie('PHPSESSID',hash('md5',uniqid()),0);
	
	ini_set('session.name','EXPOLLID');
	ini_set('session.hash_function','sha256');
	ini_set('session.hash_bits_per_character',6);
	ini_set('session.cookie_httponly',true);
	ini_set('session.referer_check', 'your.domain.com');
	
	define("ENCRYPTION_KEY", "YOUR-CUSTOM-SALTKEY");
	
	define('BASE_PATH',realpath('.'));
	define('BASE_URL', dirname($_SERVER["SCRIPT_NAME"]));
	
	define('SESS_KEY_POLLID','poll_id');
	define('SESS_KEY_SPAM','spam');
	define('SESS_KEY_URL_PREVIOUS','url_previous');
	
	define('SESS_KEY_H_HTTPUSERAGENT','hashed_httpUserAgent');
	define('SESS_KEY_H_REMOTEADDR','hashed_remoteAddr');

	define('DOCUMENT_TITLE',		'EasyXPoll');
	define('DATE_FORMAT', 			'F jS, Y, g:i a');
	
	define('ID_FRONTPAGE_POLL',		'POLL_ID');
	
	define('DB_TYPE', 				'mongodb');
	define('DB_HOST', 				'localhost');
	define('DB_USER',				'');
	define('DB_PASSWORD',			'');
	define('DB_DATABASE',			'easyxpoll');
	define('DB_CHARSET',			'utf8');
	define('DB_PORT',				27017);
	define('DB_PRINT_ERROR',		false);
	
	define('DB_COLLECTION_POLLS',			'polls');
	define('DB_COLLECTION_POLL_VOTES',		'poll_votes');
	define('DB_COLLECTION_POLL_STATISTICS',	'poll_statistics');
	
	define('LENGTH_MAX_QUESTION', 		96);
	define('LENGTH_MAX_ANSWER', 		32);
	define('LENGTH_MAX_URL', 			255);
	
	define('LENGTH_MIN_QUESTION', 		10);
	define('LENGTH_MIN_ANSWER', 		2);
	define('LENGTH_MIN_URL', 			11);
	
	define('COUNT_MAX_OPTIONS', 		8);

