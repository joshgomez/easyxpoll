<?php 

	if(!IN_SITE)
	{
		exit;
	}
	
	class Configs
    {
        const DBEXT_MONGO = 1;
		
		private static $dbExt;
		private static $pollColorThemes = [
		
			['css' => 'default', 			'name' => 'Default' ],
			['css' => 'electric_crimson', 	'name' => 'Electric Crimson' ],
			['css' => 'amazon', 			'name' => 'Amazon' ],
			['css' => 'fallow', 			'name' => 'Fallow' ],
			['css' => 'bronze', 			'name' => 'Bronze' ],
			['css' => 'mint', 				'name' => 'Mint' ],
			['css' => 'lavender', 			'name' => 'Lavender' ],
			['css' => 'amber', 				'name' => 'Amber' ],
			['css' => 'aureolin', 			'name' => 'Aureolin' ],
			['css' => 'air_force_blue', 	'name' => 'Air Force Blue' ], 
			['css' => 'blue', 				'name' => 'Blue' ],
			['css' => 'red', 				'name' => 'Red' ],
			['css' => 'violet', 			'name' => 'Violet' ],
			['css' => 'green', 				'name' => 'Green' ]
		];
		
		static public function pollColorThemes($options = null)
		{
			if(isset($options['asort']))
				asort(static::$pollColorThemes);
			
			return static::$pollColorThemes;
		}
		
		static public function getDatabaseExtension()
		{
			switch(DB_TYPE)
			{

				default:
					static::$dbExt = self::DBEXT_MONGO;
			}
			
			return static::$dbExt;
		}
		
		private function __construct(){}
		private function __clone(){}
    }
	
	