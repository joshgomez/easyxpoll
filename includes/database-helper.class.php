<?php

	if(!IN_SITE)
	{
		exit;
	}

	abstract class DatabaseHelper extends DatabaseLayer
	{
		public function findAllStatistics()
		{
			$stats = $this->get(DB_COLLECTION_POLL_STATISTICS,[],null,true);
			if($stats){
			
				$arr = [];
				foreach($stats as $value)
				{
					$arr[] = $this->getArray($value);
				}
				
				return $arr;
			}
			
			return false;
		}
	
		public function findVoteByIp($id)
		{
			$id 		= $id;
			$ip 		= inet_pton(getIP());
		
			$this->bindType($id,'I');
			$this->bindType($ip,'l');
			
			if($id)
			{
				$poll = $this->get(DB_COLLECTION_POLL_VOTES,['id_poll' => $id, 'ip' => $ip]);
				if($poll)
					return $this->getArray($poll);
			}
				
			return false;
		}
		
		public function updatePollStatsOnCreate($polls)
		{
			$polls = $polls;
			$lastPoll = time();
			
			$this->bindType($polls,'i');
			$this->bindType($lastPoll,'t');
			
			try
			{
				$this->update(DB_COLLECTION_POLL_STATISTICS,['name' => 'polls_created'],['value' => $polls]);
				$this->update(DB_COLLECTION_POLL_STATISTICS,['name' => 'last_poll'],['value' => $lastPoll]);
				return true;
			}
			catch(Exception $e){}
			
			return false;
		}
		
		public function findPollById($id)
		{
			$id = $id;
			$id = $this->bindType($id,'I');
			
			if($id)
			{
				$poll = $this->get(DB_COLLECTION_POLLS,['_id' => $id]);
				if($poll)
					return $this->getArray($poll);
			}
				
			return false;
		}
	
		public function insertPoll($question,array $answers,$theme)
		{
			$ip 		= inet_pton(getIP());
			$accesskey 	= randomPassword();
			
			$cipher = new Cipher(ENCRYPTION_KEY);
			$accesskey = $cipher->encrypt($accesskey);
		
			$question 	= $question;
			$theme 		= $theme;
			$votes 		= [];
			
			$callback = function($value) use (&$votes){
			
				$num = 0;
				$votes[] = $this->bindType($num,'i');
				return $this->bindType($value,'s');
			};		
		
			$answers = array_map($callback,$answers);
			
			$closed 	= 0;
			$hidden 	= 0;
			$updated 	= 0;
			$created = time();
		
			$this->bindType($ip,'l');
			$this->bindType($accesskey,'s');
			
			$this->bindType($question,'s');
			$this->bindType($theme,'i');
			$this->bindType($answers,'a');
			$this->bindType($votes,'a');
			$this->bindType($closed,'i');
			$this->bindType($hidden,'i');
			$this->bindType($updated,'t');
			$this->bindType($created,'t');
			
			$data = [
				'ip' => $ip,
				'accesskey' => $accesskey,
				'question' => $question, 
				'theme' => $theme, 
				'answers' => $answers, 
				'votes' => $votes, 
				'closed' => $closed, 
				'hidden' => $hidden, 
				'updated' => $updated, 
				'created' => $created
			];
			
			return $this->insert(DB_COLLECTION_POLLS,$data);
		}
		
		public function updatePollVotes($id,$answer,array $votes)
		{
			$id = $id;
			$votes = $votes;
			
			$this->bindType($id,'I');
			if($id && array_key_exists($answer,$votes)){

				$updated = time();
				$num = $votes[$answer] + 1;
				$votes[$answer] = $this->bindType($num,'i');
				
				$this->bindType($votes,'a');
				$this->bindType($updated,'t');
				
				if($this->update(DB_COLLECTION_POLLS,['_id' => $id],['votes' => $votes, 'updated' => $updated]))
					return true;
			}
			
			return false;
		}
		
		public function updatePoll($id,$theme,$closed)
		{
			$id = $id;
			$theme = $theme;
			$closed = $closed;
			
			$this->bindType($id,'I');
			if($id){
			
				$this->bindType($theme,'i');
				$this->bindType($closed,'i');
				
				$data = [
				
					'theme' => $theme,
					'closed' => $closed
					
				];
				
				if($this->update(DB_COLLECTION_POLLS,['_id' => $id],$data))
					return true;
			}
			
			return false;
		}
		
		public function removePoll($id)
		{
			$id = $id;
			$this->bindType($id,'I');
			
			$votes = $this->remove(DB_COLLECTION_POLL_VOTES,['id_poll' => $id]);
			if($votes)
			{
				$hidden = 1;
				$this->bindType($hidden,'i');
				
				if($this->update(DB_COLLECTION_POLLS,['_id' => $id],['hidden' => $hidden]))
					return true;
			}
			
			return false;
		}
		
		public function insertUserVote($id_poll,$answer)
		{
			$id 		= $id_poll;
			$ip 		= inet_pton(getIP());
			$answer 	= $answer;
			$created 	= time();
			
			$this->bindType($id,'I');
			$this->bindType($ip,'l');
			$this->bindType($answer,'i');
			$this->bindType($created,'t');
			
			$data = [
				'id_poll' => $id,
				'ip' => $ip,
				'answer' => $answer, 
				'created' => $created
			];
		
			return $this->insert(DB_COLLECTION_POLL_VOTES,$data);
		}
	}
	
