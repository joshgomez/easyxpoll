
function Ajax(el)
{
	this.el = el;
	this.xmlhttp = new XMLHttpRequest();
	this.parameters = '';
	this.callbackParameters = [];
	this.callback = null;
}

Ajax.prototype = {

	get:function(url){
	
		this.xmlhttp.open('GET',url,true);
		this.xmlhttp.send(null);
	},
	
	reset:function(){
	
		this.parameters = '';
	},

	add:function(name,value){
	
		if(this.parameters.length > 0)
			this.parameters += '&';
		
		this.parameters += name + '=' + encodeURIComponent(value);
	},
	
	post:function(url){
	
		var length = this.parameters.length;
		if(length > 0){
		
			this.xmlhttp.open('POST',url,true);
			this.xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			//this.xmlhttp.setRequestHeader("Content-length",length);
			//this.xmlhttp.setRequestHeader("Connection","close");
			this.xmlhttp.send(this.parameters);
			
			return true;
		}
		else
			alert('AJAX: No parameters are set! (POST)');
			
		return false;
	},
	
	setStateChange:function(){
	
		var self = this;
		this.xmlhttp.addEventListener('readystatechange',function(e){
		
			if(self.xmlhttp.readyState == 4 && self.xmlhttp.status == 200){
			
				document.getElementById(self.el).innerHTML = self.xmlhttp.responseText;
				if(self.callback){
				
					if(self.callbackParameters.length > 0)
						self.callback.apply(this, self.callbackParameters);
					else
						self.callback.call(this);
				}
			}
		});
	}
};

(function()
{
	var iframePoll = document.getElementById('iframePoll');
	if(iframePoll){

		function reDrawIframe()
		{
			var iframeDoc = (iframePoll.contentDocument) ? iframePoll.contentDocument : iframePoll.contentWindow.document;
			var iframeHTML = (iframeDoc.documentElement) ? iframeDoc.documentElement : iframeDoc.getElementsByTagName('html')[0];
			
			if(iframeHTML)
			{
				var iframeBody = (iframeDoc.body) ? iframeDoc.body : iframeDoc.getElementsByTagName('body')[0];
				var iframeHeight = Math.max
				( 
					iframeBody.scrollHeight, 
					iframeBody.offsetHeight, 
					iframeHTML.clientHeight, 
					iframeHTML.scrollHeight, 
					iframeHTML.offsetHeight
				);
				
				iframePoll.style.overflow 	= 'hidden';
				iframePoll.style.visibility = 'hidden';
				iframePoll.style.height 	= "10px";
				
				iframePoll.style.height 	= iframeHeight + 4 + "px";
				iframePoll.style.visibility = 'visible';
				iframePoll.scrolling = 'no';
				iframePoll.verticalscrolling = 'no';
				iframePoll.scrollbars = 'no';
				
				var textareaPoll = document.getElementById('textareaPoll');
				if(textareaPoll){
				
					var textareaBody = '' + 
					'<iframe ' + '\n\tsrc="' + iframePoll.src + '" scrolling="no" allowtransparency="true" frameborder="0"' + 
					'\n\tstyle="overflow:hidden;width:100%;height:' + (iframeHeight + 4) + 'px;" seamless>\n</iframe>';

					textareaPoll.value = textareaBody;
				}
			}
		}
		
		iframePoll.addEventListener('load',function(e){
			reDrawIframe();
		});
		
		reDrawIframe();
	}

	var formPoll = document.getElementById('formPoll');
	if(formPoll)
	{
		var elmAnswers = document.getElementById('inputAnswerContainer');
		var totalAnswers = elmAnswers.getElementsByTagName('div').length;
		var selectedAnswers = formPoll.answers.value;
		var elmInputs = formPoll.getElementsByTagName('input');
		
		formPoll.select.style.display = 'none';
		formPoll.question.required = true;
		
		elmInputs[1].required = true;
		elmInputs[2].required = true;
		
		function updateAnswerFields()
		{
			if(selectedAnswers != formPoll.answers.value)
			{
				selectedAnswers = formPoll.answers.value;
			
				for(var i = selectedAnswers;i <= totalAnswers;i++)
				{
					elmInputs[i].parentNode.style.display = 'none';
					elmInputs[i].required = false;
				}
			
				for(var i = 1;i <= selectedAnswers;i++)
				{
					elmInputs[i].parentNode.style.display = 'block';
					elmInputs[i].required = true;
				}
			}
		}
		
		function hidePreviewIcon()
		{
			document.getElementById('pollPreviewIcon').style.display = 'none';
		}
		
		var ajaxPreview = new Ajax('preview');
		var timerPreview;
		function getPreview()
		{
			var cssPollTheme = document.getElementById('cssPollTheme');
			if(cssPollTheme){
			
				var selectedOption = formPoll.theme.options[formPoll.theme.selectedIndex];
				cssPollTheme.href = g_hostURL + '/styles/poll-theme-' +  
					encodeURIComponent(selectedOption.getAttribute('data-css')) + '.min.css';
			}
		
			var preview = document.getElementById('preview');
			
			document.getElementById('pollPreviewIcon').style.display = 'inline-block';
			
			if(timerPreview)
				clearTimeout(timerPreview);
				
			timerPreview = setTimeout(function(){
			
				ajaxPreview.callback = hidePreviewIcon;
				ajaxPreview.setStateChange();
				
				ajaxPreview.reset();
				
				ajaxPreview.add(formPoll.question.name,formPoll.question.value);
				ajaxPreview.add(formPoll.theme.name,formPoll.theme.value);
				
				for(var i = 1;(i-1) < selectedAnswers;i++)
				{
					ajaxPreview.add('answer[]',elmInputs[i].value);
				}
				
				ajaxPreview.post(g_hostURL + '/ajax.php?page=preview');
			
			},500);
		}
		
		formPoll.addEventListener('change',function(e){
		
			updateAnswerFields();
			getPreview();

		});
	}
	
	var navPreviewBG = document.getElementById('navPreviewBG');
	if(navPreviewBG)
	{
		navPreviewBG.addEventListener('click',function(e){
		
			var t = e.target;
			if(t.nodeName == 'A')
			{
				var dataBG = t.getAttribute('data-bg');
				if(dataBG)
				{
					var parent = navPreviewBG.parentNode;
					parent.style.background = t.getAttribute('data-bg');
				}
			}
		});
	}
	
	
}());

//-----------------------------------------------------------------------------------------------------------------------------------
