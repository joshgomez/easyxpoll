<?php

	define('IN_SITE',true);
	
	require __DIR__ . '/includes/main.php';
	setSpamFilter('javascript');
	
	$offset = 3600 * 0;
	$etag = hash_file('md5',$_SERVER['SCRIPT_FILENAME']);

	ob_start('ob_gzhandler');
	
	setCacheHeader($etag,$offset);
	header('Content-Type: application/x-javascript; charset=utf-8');

	require __DIR__ . '/javascripts/main.php.js';
	require __DIR__ . '/javascripts/main.min.js';
	
	ob_end_flush();

?>