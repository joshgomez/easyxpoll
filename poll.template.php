<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<div>
	<form id='formVote' action='#' method='POST'>
		<p class='easyxpoll-question'><?=sanitizeSpChars($question)?></p>
		<?php 
		
			foreach($answers as $i => $answer): 
			
				$percent = 0;
				if($totalVotes > 0)
					$percent = round($votes[$i]/$totalVotes*100);
		
		?>
		
			<div class='easyxpoll-answer'>
				<?php if($hasVoted || $closed): ?>
				
				<label><?=($i+1).'. '.sanitizeSpChars($answer)?></label>
				
				<?php else: ?>
					
				<input id='inputAnswer<?=$i?>' type='radio' name='answer' value='<?=$i?>' />
				<label for='inputAnswer<?=$i?>'><?=($i+1).'. '.sanitizeSpChars($answer)?></label>
					
				<?php endif; ?>
			</div>
			
			<div class='easyxpoll-bar-container'>
				<div class='easyxpoll-bar' 
					style='width:<?=$percent?>%;' 
					title='<?=$percent?>%'><?=$votes[$i]?></div>
			</div>
		
		<?php endforeach; ?>
		
		<div>
			<input type='hidden' name='id' value='<?=sanitizeSpChars($id)?>' />
			<?php if(!$hasVoted): ?>
			
			<?php if(!$closed): ?>
			
			<button type='submit' name='vote' class='easyxpoll-button' 
				value='<?=_translate('VOTE')?>'<?=(isset($disabled) ? ' disabled' : '')?>><?=_translate('VOTE')?></button>
			
			<?php else: ?> <span class='easyxpoll-closed'><?=_translate('TXT_CLOSED')?></span>
			<?php endif; ?>
			
			<?php else: ?>
			<span class='easyxpoll-voted'><?=_translate('TXT_VOTED')?></span>
			<?php endif; ?>
		</div>
	</form>
</div>