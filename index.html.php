<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title><?=((!empty($g_title)) ? sanitizeSpChars($g_title) .' - ' : ''). DOCUMENT_TITLE?></title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<meta name="keywords" content="<?=$g_htmlKeywords?>" />
		<meta name="description" content="<?=$g_htmlDescription?>" />
			
		<meta name="author" content="Josh" />
		<meta name="generator" content="HTML,Javascript,PHP" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		
		<link rel="stylesheet" href="<?=$g_hostURL?>/styles/main.min.css" />
		<link id='cssPollTheme' rel="stylesheet" href="<?=$g_hostURL?>/styles/poll-theme-default.min.css" />
		<link rel="stylesheet" href="<?=$g_hostURL?>/styles/ionicons.min.css" />
		<link rel='icon' type='image/vnd.microsoft.icon' href='<?=$g_hostURL?>/favicon.ico' />
		
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="<?=$g_hostURL?>/javascripts/respond.min.js"></script>
		<![endif]-->
		
		<?php require __DIR__ . '/includes/thirdparties/analyticstracking.php'; ?>
	</head>
	<body>
		<div id='container'>
			<header id='header'>
				<a href='<?=$g_hostURL?>'>
					<h1><?=DOCUMENT_TITLE?></h1>
					<p><?=_translate('TXT_SLOGAN')?></p>
				</a>
			</header>
			<div id='content'>
				<?php require __DIR__ . '/pages-output/' . $g_getPage . '.php'; ?>
			</div>
			
			<div id='footer_separator'></div>
			
			<footer id='footer'>
				<p><?=DOCUMENT_TITLE?> 2014 Josh</p>
				<a href='http://www.xuniver.se/' target='_blank'>Xuniverse</a> 
				<a href='http://www.xuniver.se/page/projects#easyxpoll' target='_blank'>Source</a>
			</footer>
		</div>
		<script src="<?=$g_hostURL?>/javascript.php"></script>
	</body>
</html>

