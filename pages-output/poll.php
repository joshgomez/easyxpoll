<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
	if($haveAccess):
	
?>

<div class='alignleft'>
	<div class='box'>
		<h2>
			<?=_translate('YOUR_POLL','UC')?>
			<a class='alignright ' href='<?=$g_hostURL.'/page/logout'?>'><?=_translate('LOGOUT')?></a>
		</h2>
		<div class='padding'>
		
			<iframe id="iframePoll" frameborder="0" allowtransparency="true" 
				style="width:100%"
				src="<?=$g_hostURL.'/id/'.$poll->id()?>" seamless>
			</iframe>
			
		</div>
		<nav id='navPreviewBG'>
			<ul>
				<li><a style='background:black' href='#' data-bg='black'></a></li>
				<li><a style='background:white' href='#' data-bg='white'></a></li>
			</ul>
		</nav>
	</div>

	<div class='box'>
		<h2><?=_translate('SETTINGS','UC')?></h2>
		<div class='padding'>
			<form id='formEdit' action='<?=$g_hostURL.'/page/poll/'?>' method='POST'>
				<div>
					<div>
						<label for='selectTheme'><?=_translate('THEME')?></label>
						<select id='selectTheme' name='theme'>
							<?php
							
							foreach(Configs::pollColorThemes(['asort' => true]) as $i => $theme):
								$selected_theme = '';
								if($poll->theme() == $i)
									$selected_theme = ' selected';
									
							?>
							
								<option value='<?=$i?>'<?=$selected_theme?>><?=$theme['name']?></option>
							<?php endforeach; ?>
							
						</select> 
					</div>
					<div>
						<fieldset class='radioLabels'>
							<legend><?=_translate('CLOSE')?></legend>
							<?php 
							
							foreach($closeInputs as $i => $value):
							
							$checked_close = "";
							if($poll->closed() == $i) $checked_close = " checked";
							
							?>
							
							<label for='inputClose<?=$i?>'><?=$value?></label>
							<input id='inputClose<?=$i?>' type='radio' name='close' value='<?=$i?>'<?=$checked_close?> />
						
							<?php endforeach; ?>
						</fieldset>
					</div>
					<div>
						<button type='submit' name='edit' value='<?=_translate('EDIT')?>'><?=_translate('EDIT')?></button>
						<button class='alignright' type='submit' name='delete' 
							value='<?=_translate('DELETE')?>'><?=_translate('DELETE')?></button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div class='alignright'>
	<div class='box'>
		<h2><?=_translate('YOUR_CODE','UC')?></h2>
		<div class='padding textcenter'>
			<textarea id='textareaPoll'><?=sanitizeSpChars($iframeHTML)?></textarea>
			<code><?=sanitizeSpChars($poll->id())?></code>
			<br />
			<br />
			<p><?=sprintf(_translate('TXT_POLL_KEY'),$accesskey)?></p>
		</div>
	</div>

	<div class='box'>
		<h2><?=_translate('STATISTICS','UC')?></h2>
		<div class='padding'>
			<dl>
			  <dt><?=_translate('VOTES')?></dt>
			  <dd><?=array_sum($poll->votes())?></dd>
			
			  <dt><?=_translate('LAST_VOTED')?></dt>
			  <dd><?=($poll->updated() > 0 ? date(DATE_FORMAT,$poll->updated()) : _translate('NONE'))?></dd>
			  
			  <dt><?=_translate('DATE_CREATED')?></dt>
			  <dd><?=date(DATE_FORMAT,$poll->created())?></dd>
			</dl>
		</div>
	</div>

	<?php require __DIR__ . '/includes/ads_banner.php'; ?>
	
</div>

<?php else:?>

<div class='widebox'>
	<?php $errNoPoll = true; require __DIR__ . '/includes/authenticate.php'; ?>
</div>

<?php endif;?>

