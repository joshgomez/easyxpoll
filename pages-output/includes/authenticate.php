<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}
	
?>

<h2><?=_translate('YOUR_POLL','UC')?></h2>
<div class='padding'>
	<form id='formAuthenticate' action='<?=$g_hostURL?>/page/poll' method='POST'>
		<div>
			<div>
				<label for='inputId'><?=_translate('ID')?></label>
				<?php if(isset($errNoPoll)): ?><p class='inputDescription failure'><?=_translate('TXT_POLL_NONE')?></p><?php endif;?>
				<input id='inputId' type='text' name='id' />
			</div>
			<div>
				<label for='inputKey'><?=_translate('KEY')?></label>
				<input id='inputKey' type='password' name='key' />
				<span class='inputOptional'>(<?=_translate('OPTIONAL')?>)</span>
			</div>
			<div>
				<button type='submit' name='authenticate' value='<?=_translate('EDIT')?>'><?=_translate('EDIT')?></button>
			</div>
		</div>
	</form>
</div>