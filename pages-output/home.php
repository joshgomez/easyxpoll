<?php

	if(!defined('IN_SITE'))
	{
		exit;
	}

?>

<div class='alignleft'>
	<div class='box'>
		<h2><?=_translate('CREATE','UC')?></h2>
		<div class='padding'>	
			<form id='formPoll' action="<?=$g_hostURL?>/" method='POST'>
				<div>
					<div>
						<label for='inputQuestion'><?=_translate('QUESTION')?></label>
						<p class='inputDescription<?=$classQuestion?>'><?=$descQuestion?></p>
						<input name='question' id='inputQuestion' pattern='.{<?=LENGTH_MIN_QUESTION?>,}' value='<?=$question?>' 
							title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_QUESTION)?>' maxlength="<?=LENGTH_MAX_QUESTION?>">
					</div>
					<div>
						<label for='selectTheme'><?=_translate('THEME')?></label>
						<select id='selectTheme' name='theme'>
							<?php
							
							foreach(Configs::pollColorThemes(['asort' => true]) as $i => $theme):
								$selected_theme = '';
								if($i == $selectedTheme)
									$selected_theme = ' selected';
									
							?>
							
								<option data-css='<?=$theme['css']?>' 
									value='<?=$i?>'<?=$selected_theme?>><?=$theme['name']?></option>
							<?php endforeach; ?>
							
						</select> 
					</div>

					<div>
						<label for='selectAnswers'><?=_translate('ANSWERS')?></label>
						<select id='selectAnswers' name='answers'>
							<?php
							
							for($i = 2;$i <= COUNT_MAX_OPTIONS;$i++):
								$selected_answers = '';
								if($i == $selectedAnswers)
									$selected_answers = 'selected';
									
							?>
							
								<option <?=$selected_answers?>><?=$i?></option>
							<?php endfor; ?>
							
						</select> 
						<button type='submit' class='nomargin' name='select' value='<?=_translate('SELECT')?>'><?=_translate('SELECT')?></button>
					</div>
					<fieldset id='inputAnswerContainer'>
						<legend><?=_translate('ANSWERS')?></legend>
						<p class='inputDescription<?=$classAnswer?>'><?=$descAnswer?></p>
						<?php 
						
						for($i = 1;$i <= COUNT_MAX_OPTIONS;$i++):
						
							$class_answer = '';
							if($i > $selectedAnswers)
							{
								$class_answer = 'class="hidden"';
							}
						
						?>
						
						<div <?=$class_answer?>>
							<label for='inputAnswer<?=$i?>'><?=_translate('ANSWER').' '.$i?></label>
							<input name="answer[]" id='inputAnswer<?=$i?>' pattern='.{<?=LENGTH_MIN_ANSWER?>,}'  value='<?=$answers[$i-1]?>' 
								title='<?=sprintf(_translate('TXT_MIN_LENGTH'),LENGTH_MIN_ANSWER)?>' maxlength="<?=LENGTH_MAX_ANSWER?>">
						</div>
						
						<?php endfor; ?>
					</fieldset>
					
					<div class='si_captcha'>
						<label for='inputCaptcha'><?=_translate('CAPTCHA')?></label>
						<img id="siimage" class="si_image" 
							src="<?=$g_hostURL?>/securimage/securimage_show.php?sid=<?=md5(uniqid())?>" alt="CAPTCHA Image"> 
					</div>
					
					<div>
						<div class='si_option'>
							<object type="application/x-shockwave-flash" class="si_audio" 
								data="<?=$g_hostURL?>/securimage/securimage_play.swf?icon_file=<?=$g_hostURL?>/securimage/images/audio_icon.png&amp;audio_file=<?=$g_hostURL?>/securimage/securimage_play.php">
								
								
								<param name="movie" value="<?=$g_hostURL?>/securimage/securimage_play.swf?icon_file=<?=$g_hostURL?>/securimage/images/audio_icon.png&amp;audio_file=<?=$g_hostURL?>/securimage/securimage_play.php">
							</object>
						</div>
						 
						<div class='si_option'>
							<a tabindex="-1" href="#" title="Refresh Image" 
								onclick="document.getElementById('siimage').src = '<?=$g_hostURL?>/securimage/securimage_show.php?sid=' + Math.random(); this.blur(); return false">
									<img src="<?=$g_hostURL?>/securimage/images/refresh.png" class='si_refresh' alt="Reload Image">
							</a>
						</div>
					</div>
					
					<div class='si_captchaInput'>
						<label for='inputCaptcha'><?=_translate('ENTER_CODE')?></label>
						<p class='inputDescription<?=$classCaptcha?>'><?=$descCaptcha?></p>
						<input name="captcha" maxlength="16" placeholder='<?=_translate('TXT_CAPTCHA_PLACEHOLDER')?>' id="inputCaptcha">
					</div>
					
					<div>
						<button type='submit' name='create' value='<?=_translate('CREATE')?>'><?=_translate('CREATE')?></button>
					</div>
				</div>
			</form>
		</div>
	</div>
	
	<?php require __DIR__ . '/includes/ads_banner.php'; ?>
	
</div>

<div class='alignright'>
	<div class='box'>
		<h2><?=_translate('PREVIEW','UC')?></h2>
		<div class='padding textcenter'>
			<i id="pollPreviewIcon" class='ion-loading-d' title='<?=_translate('LOADING')?>'></i>
			<div id='preview'>
				<iframe id="iframePoll" frameborder="0" allowtransparency="true" 
					style="width:100%"
					src="<?=$g_hostURL.'/id/'.ID_FRONTPAGE_POLL?>" seamless>
				</iframe>
			</div>
		</div>
		<nav id='navPreviewBG'>
			<ul>
				<li><a style='background:black' href='#' data-bg='black'></a></li>
				<li><a style='background:white' href='#' data-bg='white'></a></li>
			</ul>
		</nav>
	</div>

	<div class='box'>
	<h2><?=_translate('STATISTICS','UC')?></h2>
		<div class='padding'>
			<dl>
			  <dt><?=_translate('POLLS_CREATED')?></dt>
			  <dd><?=$pollStats->polls_created()?></dd>
			
			  <dt><?=_translate('LAST_POLL')?></dt>
			  <dd><?=($pollStats->last_poll() > 0 ? date(DATE_FORMAT,$pollStats->last_poll()) : _translate('NONE'))?></dd>
			</dl>
		</div>
	</div>

	<div class='box'>
		<?php if($poll->id()): ?>
		
		<h2><?=_translate('YOUR_POLL','UC')?></h2>
		<div class='padding'>
			<p><?=sanitizeSpChars($poll->question())?></p>
			<a href='<?=$g_hostURL.'/page/poll'?>'><?=_translate('EDIT')?></a> | 
			<a href='<?=$g_hostURL.'/page/logout'?>'><?=_translate('LOGOUT')?></a>
		</div>
		
		<?php else: ?>
			<?php require __DIR__ . '/includes/authenticate.php'; ?>
		<?php endif; ?>
		
	</div>
</div>
	
