<?php

	/*
		Tested on PHP 5.5.8 with 
		Curl
		Memcached Session
		GeoIP 1.0.8
		MySQLnd, PDO_MySQL (MySQL 5.6.15)
		XCache
	*/
	
	define('IN_SITE',true);
	
	require __DIR__ . '/includes/main.php';
	setSpamFilter('index');
	
	$g_getPage = ifSetOr($_GET['page'],'home');
	$g_getPage = page($g_getPage);
	
	$g_htmlKeywords 	= _translate('HTML_KEYWORDS');
	$g_htmlDescription 	= _translate('HTML_DESCRIPTION');
	
	require __DIR__ . '/pages/' . $g_getPage . '.php';
	
	require __DIR__ . '/index.html.php';
	
	