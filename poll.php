<?php

	define('IN_SITE',true);
	
	require __DIR__ . '/includes/main.php';
	setSpamFilter('poll');
	
	$g_htmlKeywords 	= _translate('HTML_KEYWORDS');
	$g_htmlDescription 	= _translate('HTML_DESCRIPTION_POLL');
	
	$themes = Configs::pollColorThemes();
	
	$question 	= '';
	$theme 		= $themes[0];
	$answers 	= [];
	$votes 		= [];
	$totalVotes = 0;
	$hasVoted = false;
	$closed 	= 0;
	
	$style_body 			= '';
	$style_bar 				= '';
	$style_barContainer 	= '';
	
	$style_question = '';
	$style_answers  = '';
	$style_closed  	= '';
	$style_voted  	= '';
	
	$id = ifSetOr($_REQUEST['id']);
	$poll->getById($id);
	if($poll->id() && $poll->hidden() === 0)
	{
		$id 		= $poll->id();
		$question 	= $poll->question();
		$theme 		= ifSetOr($themes[$poll->theme()],$themes[0]);
		$answers 	= $poll->answers();
		$hasVoted 	= $poll->hasVoted();
		$closed 	= $poll->closed();
		
		$votes = $poll->votes();
		$totalVotes = array_sum($votes);
	
		$vote = ifSetOr($_POST['vote']);
		if($vote)
		{
			$answer = ifSetOr($_POST['answer']);
			if($answer !== null){
			
				$voted = $poll->vote($answer);
				if($voted)
				{
					$votes[$answer] 	+= 1;
					$totalVotes 		+= 1;
					$hasVoted 			= true;
				}
			}
		}
	}
	else
	{
		$id = '';
	}
	
	require __DIR__ . '/poll.html.php';
	
